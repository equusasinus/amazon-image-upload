package com.equus.asinus.profile;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

@Data
@AllArgsConstructor
public class UserProfile {
    private final UUID userProfileId;
    private final String username;
    private String profileImageKey;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserProfile that = (UserProfile) o;
        return Objects.equals(userProfileId, that.userProfileId) &&
                Objects.equals(username, that.username) &&
                Objects.equals(profileImageKey, that.profileImageKey);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userProfileId, username, profileImageKey);
    }

    public Optional<String> getProfileImageKey() {
        return Optional.ofNullable(profileImageKey);
    }
}
