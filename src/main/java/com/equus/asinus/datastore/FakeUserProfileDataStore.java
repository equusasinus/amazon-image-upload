package com.equus.asinus.datastore;

import com.equus.asinus.profile.UserProfile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Component
public class FakeUserProfileDataStore {
    private static final List<UserProfile> USER_PROFILES = new ArrayList<>();

    static {
        USER_PROFILES.add(new UserProfile(UUID.fromString("07c37224-3823-480e-a86a-1e77e1ea7212"),"equus",null));
        USER_PROFILES.add(new UserProfile(UUID.fromString("e91749be-e01d-4186-aeea-1140a1e60ade"),"asinus",null));

    }

    public List<UserProfile> getUserProfiles(){
        return USER_PROFILES;
    }
}
