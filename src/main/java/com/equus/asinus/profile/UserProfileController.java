package com.equus.asinus.profile;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.swing.text.html.Option;
import java.util.*;

@RestController
@RequestMapping("api/v1/user-profile")
@CrossOrigin("*")
public class UserProfileController {
    private final UserProfileService userProfileService;

    @Autowired
    public UserProfileController(UserProfileService userProfileService) {
        this.userProfileService = userProfileService;
    }

    @GetMapping()
    public List<UserProfile> getUserProfiles(){
        return userProfileService.getUserProfiles();
    }

    @PostMapping(
            path = "{userProfileId}/image/upload",
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<Map<String, String>> uploadUserProfileImage(@PathVariable("userProfileId") String userProfileId,
                                                 @RequestParam("file") MultipartFile file){
        userProfileService.uploadUserProfileImage(UUID.fromString(userProfileId),file);
        Map<String, String> response = new HashMap<>();
        response.put("success", "true");
        return ResponseEntity.of(Optional.of(response));
    }

    @GetMapping(value = "{userProfileId}/image/download", produces = MediaType.IMAGE_JPEG_VALUE)
    public byte[] downlodUserProfileImage(@PathVariable String userProfileId){
        return userProfileService.downlodUserProfileImage(UUID.fromString(userProfileId));
    }

}
