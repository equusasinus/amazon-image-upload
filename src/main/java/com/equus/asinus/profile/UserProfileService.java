package com.equus.asinus.profile;

import com.equus.asinus.bucket.BucketName;
import com.equus.asinus.filestore.FileStore;
import org.apache.http.entity.ContentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;
import java.util.stream.Stream;

@Service
public class UserProfileService {

    private final UserProfileDataAccessService userProfileDataAccessService;
    private final FileStore fileStore;


    @Autowired
    public UserProfileService(UserProfileDataAccessService userProfileDataAccessService, FileStore fileStore) {
        this.userProfileDataAccessService = userProfileDataAccessService;
        this.fileStore = fileStore;
    }

    List<UserProfile> getUserProfiles(){
        return userProfileDataAccessService.getUserProfiles();
    }

    void uploadUserProfileImage(UUID userProfileId, MultipartFile file) {
        //check if image is not empty
        if (file == null) {
            throw new IllegalStateException("No file to upload");
        }
        if (file.isEmpty()) {
            throw new IllegalStateException("Cannot upload image file [" + file.getSize() + "]");
        }
        // if file is an image
        String contentType = file.getContentType();
        if (isImage(contentType)){
            throw new IllegalStateException("file must be an image " + contentType);
        }
        // the user exists in our database
        UserProfile userProfile = getUserProfileOrThrow(userProfileId);
        // grab some metadata from file if any
        Map<String, String> metaData = getMetaData(file);

        // store the image in s3 and update database with s3 image link
        String path = String.format("%s/%s", BucketName.PROFILE_IMAGE.getBucketName(),userProfile.getUserProfileId());
        String filename = String.format("%s", UUID.randomUUID());
        try {
            fileStore.save(path,filename,Optional.of(metaData), file.getInputStream());
            userProfile.setProfileImageKey(filename);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

    }

    public byte[] downlodUserProfileImage(UUID userProfileId){
        UserProfile user = getUserProfileOrThrow(userProfileId);
        String path = String.format("%s/%s",
                BucketName.PROFILE_IMAGE.getBucketName(),
                user.getUserProfileId());

        return user.getProfileImageKey()
                .map(key -> fileStore.download(path, key))
                .orElse(new byte[0]);
    }

    private UserProfile getUserProfileOrThrow(UUID userProfileId){
        return userProfileDataAccessService.getUserProfiles()
                .stream()
                .filter(element ->  element.getUserProfileId().equals(userProfileId))
                .findFirst()
                .orElseThrow(() -> new IllegalStateException(String.format("User profile %s not found", userProfileId)));
    }

    private Map<String, String> getMetaData(MultipartFile file){
        Map<String, String> metaData = new HashMap<>();
        metaData.put("Content-Type", file.getContentType());
        metaData.put("Content-Length", String.valueOf(file.getSize()));
        return metaData;
    }

    private boolean isImage(String contentType){
        Optional<String> type = Stream.of(ContentType.IMAGE_JPEG.getMimeType(), ContentType.IMAGE_GIF.getMimeType(), ContentType.IMAGE_PNG.getMimeType())
                .filter(e-> e.contains(contentType))
                .findFirst();
        return type.isEmpty();
    }
}
