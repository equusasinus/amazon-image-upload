package com.equus.asinus.bucket;

public enum BucketName {
    PROFILE_IMAGE("com.equus.asinus");

    private final String bucketName;

    BucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    public String getBucketName() {
        return bucketName;
    }
}
